/* See LICENSE file for copyright and license details. */

#ifndef PSYQ2ELF_ELF_OBJ_H
#define PSYQ2ELF_ELF_OBJ_H

#include <stdint.h>
#include <stdio.h>

#include <elf.h>

#include "psyq_obj.h"
#include "slist.h"

typedef struct elf_sym {
	Elf32_Sym sym;
	const char *name;
	psyq_section *psection;
	psyq_sym *psym;
} elf_sym;

SLIST_DECL(elf_sym);

typedef struct elf_section {
	Elf32_Shdr shdr;
	const char *name;
	uint8_t *data;
	uint32_t size;
	uint32_t bss_offset;
	uint32_t sym_index;
	psyq_section *psection;
} elf_section;

SLIST_DECL(elf_section);

typedef struct elf_file {
	slist_elf_section sections;
	slist_elf_sym syms;
} elf_file;

int elf_convert_link(FILE *file, psyq_link *link);

#endif
