/* See LICENSE file for copyright and license details. */

#include <stdio.h>

#include "elf_obj.h"
#include "psyq_obj.h"

static const char *argv0;

static int
psyq2elf(const char *psyq, const char *elf)
{
	psyq_link link;
	FILE *file;

	if ((file = fopen(psyq, "rb")) == NULL)
	{
		fprintf(stderr, "Failed to open %s for reading.\n", psyq);
		return 1;
	}

	if (psyq_parse_link(file, &link))
	{
		fprintf(stderr, "psyq_parse_link failed for %s.\n", psyq);
		return 1;
	}

	if (fclose(file))
	{
		fprintf(stderr, "Failed to close %s\n.", psyq);
		return 1;
	}

	if ((file = fopen(elf, "wb")) == NULL)
	{
		fprintf(stderr, "Failed to open %s for writing.\n", elf);
		return 1;
	}

	if (elf_convert_link(file, &link))
	{
		fprintf(stderr, "elf_convert_link failed for %s.\n", psyq);
		return 1;
	}

	if (fclose(file))
	{
		fprintf(stderr, "Failed to close %s.\n", elf);
		return 1;
	}

	return 0;
}

static void
usage(void)
{
	fprintf(stderr, "usage: %s <psyq object> <elf object>\n", argv0);
}

int
main(int argc, char *argv[])
{
	argv0 = argv[0];

	if (argc != 3) {
		usage();
		return 1;
	}

	return psyq2elf(argv[1], argv[2]);
}
